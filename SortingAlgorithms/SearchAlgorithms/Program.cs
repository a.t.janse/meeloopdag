﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SearchAlgorithms
{
    class Program
    {
        const int maxValue = 16384;

        static void Main()
        {
            Console.WriteLine("Enter input");

            string[] args = Console.ReadLine().Split();

            int amountOfElements;
            while (!((args[0] == "Y" || args[0] == "N") && int.TryParse(args[1], out amountOfElements)))
            {
                Console.WriteLine("Invalid input\nExample: N 33554432");
                args = Console.ReadLine().Split();
            }
            bool showList = args[0] == "Y";
            List<int> searchList = new List<int>(amountOfElements);
            Random r = new Random();
            int value = 0;
            for (int i = 0; i < amountOfElements; i++)
            {
                value += r.Next(0, 10);
                searchList.Add(value);
            }

            if (showList) DisplayList(searchList);

            Console.WriteLine("The Algorithms:");

            while (true)
            {
                Console.WriteLine("Enter algorithm name");
                HandleInput(searchList, Console.ReadLine());
            }
        }

        static void HandleInput(List<int> elements, string input)
        {
            Random r = new Random();
            int index = r.Next(elements.Count);
            int searchData = elements[index];
            Console.WriteLine($"Searching for {searchData} on index {index}");
            Stopwatch timer = new Stopwatch();
            timer.Start();
            switch (input)
            {
                case "all":
                    break;
                case "ImplementedSearch":
                    Console.WriteLine(elements.Contains(searchData));
                    break;
                case "ImplementedBinarySearch":
                    Console.WriteLine(elements.BinarySearch(searchData) >= 0);
                    break;
                case "LinearSearch":
                    Console.WriteLine(LinearSearch(elements, searchData));
                    break;
                default:
                    Console.WriteLine("Invalid Input");
                    return;
            }

            long time = timer.ElapsedMilliseconds;
            timer.Stop();

            if (time < 1000)
                Console.WriteLine($"Elapsed time: {time} ms\n");
            else
                Console.WriteLine($"Elapsed time: {(double)time / 1000} s\n");
        }

        static void DisplayList(List<int> data)
        {
            foreach (int i in data)
            {
                Console.Write($"{i} ");
            }
            Console.Write("\n");
        }

        static bool LinearSearch(List<int> elements, int searchData)
        {
            for (int i = 0; i < elements.Count; i++)
            {
                if (elements[i] == searchData) return true;
            }
            return false;
        }
    }
}