﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortingAlgorithms
{
    class Program
    {
        /*
         * The input should consist of a character and a number.
         * The character (Y/N) is for showing the list before and afterwards.
         * The number is the amount of elements that should be sorted.
         * 
         * Example: N 33554432
         * 
         * Then you can test the different sorting algorithms by entering a string
         * 
         * BubbleSort
         * BucketSort
         * InsertionSort
         * ImplementedSort (the standard, fastest sorting algorithm, build-in by C#)
         * MergeSort
         * MonkeySort
         * PancakeSort
         * QuickSort
         * StackSort (designed by myself)
         * 
         * After it is done it will show how long it took to sort and you can enter another
         * algorithm to test it with the same unsorted list.
         * 
         * You can also start a list of algorithms after each other. Change comments in the
         * HandleInput method to chose which algorithms you want in the list.
         * 
         * Enter "Stop" to exit the program.
         */

        const int maxValue = 16384;
        
        static void Main()
        {
            Console.WriteLine("Enter input");

            string[] args = Console.ReadLine().Split();
            int amountOfElements;
            while (!((args[0] == "Y" || args[0] == "N") && int.TryParse(args[1], out amountOfElements)))
            {
                Console.WriteLine("Invalid input\nExample: N 33554432");
                args = Console.ReadLine().Split();
            }
            bool showList = args[0] == "Y";
            int[] start = new int[amountOfElements];
            Random r = new Random();
            for (int i = 0; i < amountOfElements; i++)
                start[i] = r.Next(maxValue);

            if (showList) DisplayList(start.ToList());
            
            Console.WriteLine("The Algorithms:");
            Console.WriteLine("BubbleSort, BucketSort, InsertionSort, ImplementedSort,");
            Console.WriteLine("MergeSort, MonkeySort, PancakeSort, QuickSort, StackSort");
            Console.WriteLine("Other commands: All, Stop");

            List<int> elements = new List<int>(amountOfElements);

            while (true)
            {
                elements = start.ToList();
                Console.WriteLine("Enter algorithm name");
                HandleInput(elements, Console.ReadLine().ToLower(), showList);
            }
        }

        static void HandleInput(List<int> elements, string input, bool output)
        {
            Stopwatch timer = new Stopwatch();
            timer.Start();
            switch (input)
            {
                case "all":
                    //Console.WriteLine("BubbleSort:");
                    //HandleInput(elements, "bubblesort", false);

                    Console.WriteLine("BucketSort:");
                    HandleInput(elements, "bucketsort", false);

                    //Console.WriteLine("InsertionSort:");
                    //HandleInput(elements, "insertionsort", false);

                    Console.WriteLine("ImplementedSort:");
                    HandleInput(elements, "implementedsort", false);

                    Console.WriteLine("MergeSort:");
                    HandleInput(elements, "mergesort", false);

                    //Console.WriteLine("MonkeySort:");
                    //HandleInput(elements, "monkeysort", false);

                    //Console.WriteLine("Pancake Sort:");
                    //HandleInput(elements, "pancakesort", false);

                    Console.WriteLine("QuickSort:");
                    HandleInput(elements, "quicksort", false);

                    Console.WriteLine("StackSort:");
                    HandleInput(elements, "stacksort", false);
                    return;
                case "bubblesort":
                    BubbleSort(ref elements);
                    break;
                case "bucketsort":
                    BucketSort(ref elements);
                    break;
                case "insertionsort":
                    InsertionSort(ref elements);
                    break;
                case "implementedsort":
                    elements.Sort();
                    break;
                case "mergesort":
                    StackSort(ref elements);
                    break;
                case "monkeysort":
                    MonkeySort(ref elements);
                    break;
                case "pancakesort":
                    PancakeSort(ref elements);
                    break;
                case "quicksort":
                    QuickSort(ref elements, 0, elements.Count - 1);
                    break;
                case "stacksort":
                    StackSort(ref elements);
                    break;
                case "stop":
                    Environment.Exit(0);
                    break;
                default:
                    Console.WriteLine("Invalid input");
                    return;
            }
            long time = timer.ElapsedMilliseconds;
            timer.Stop();
            if (output) DisplayList(elements);

            if (time < 1000)
                Console.WriteLine($"Elapsed time: {time} ms\n");
            else
                Console.WriteLine($"Elapsed time: {(double)time / 1000} s\n");
        }

        static void DisplayList(List<int> data)
        {
            foreach (int i in data)
            {
                Console.Write($"{i} ");
            }
            Console.Write("\n");
        }

        static bool CheckSorted(List<int> data)
        {
            for (int i = 1; i < data.Count; i++)
            {
                if (data[i - 1] > data[i]) return false;
            }
            return true;
        }

        static void BubbleSort(ref List<int> data)
        {
            int temp;
            for (int i = data.Count - 1; i >= 1 ; i--)
            {
                for (int j = 0; j < data.Count - 1; j++)
                {
                    if (data[j] > data[j+1])
                    {
                        temp = data[j];
                        data[j] = data[j + 1];
                        data[j + 1] = temp;
                    }
                }
            }
        }

        static void BucketSort(ref List<int> data)
        {
            int amountOfBuckets = (data.Count >> 16) + 1;

            List<int>[] buckets = new List<int>[amountOfBuckets];

            for (int i = 0; i < amountOfBuckets; i++)
            {
                buckets[i] = new List<int>();
            }

            for (int i = 0; i < data.Count; i++)
            {
                buckets[(data[i] * amountOfBuckets) / maxValue].Add(data[i]);
            }

            int j = 0;
            for (int i = 0; i < amountOfBuckets; i++)
            {
                buckets[i].Sort();
                foreach (int element in buckets[i])
                {
                    data[j++] = element;
                }
            }
        }

        static void InsertionSort(ref List<int> data)
        {
            int temp;
            for (int i = 1; i < data.Count; i++)
            {
                temp = data[i];
                while ((i - 1 >= 0) && (temp <= data[i - 1]))
                {
                    data[i] = data[i - 1];
                    i--;
                }
                data[i] = temp;
            }
        }

        static void MergeSort(ref List<int> data)
        {
            if (data.Count <= 1)
                return;
            List<int> left, right;
            left = data.Take(data.Count / 2).ToList();
            right = data.Skip(data.Count / 2).ToList();
            MergeSort(ref left); MergeSort(ref right);

            int l = 0, r = 0;
            for (int i = 0; i < data.Count; i++)
            {
                if (l >= left.Count)
                {
                    data[i] = right[r++];
                }
                else if (r >= right.Count)
                {
                    data[i] = left[i++];
                }
                else if (left[l] <= right[r])
                {
                    data[i] = left[l++];
                }
                else
                {
                    data[i] = right[r++];
                }
            }
        }

        static void MonkeySort(ref List<int> data)
        {
            Random r = new Random();
            while (!CheckSorted(data))
            {
                for (int i = 0; i < data.Count; i++) // swap the items at random
                {
                    int swap = r.Next(data.Count);
                    int temp = data[i];
                    data[i] = data[swap];
                    data[swap] = temp;
                }
            }
        }

        static void PancakeSort(ref List<int> data)
        {
            int maxData, maxIndex;
            for (int i = data.Count - 1; i > 0; i--)
            {
                maxData = data[i];
                maxIndex = i;
                for (int j = 0; j < i; j++)
                {
                    if (data[j] > maxData)
                    {
                        maxData = data[j];
                        maxIndex = j;
                    }
                }
                
                if (maxIndex != i)
                    data.Reverse(maxIndex, i - maxIndex + 1);
            }
        }

        static void QuickSort(ref List<int> data, int left, int right)
        {
            int l = left;
            int r = right;
            int pivot = data[(l + r) / 2];
            while (l < r)
            {
                while (data[l] < pivot)
                {
                    l++;
                }
                while (pivot < data[r])
                {
                    r--;
                }

                if (l <= r) // swap the two values
                {
                    int temp = data[l];
                    data[l] = data[r];
                    data[r] = temp;
                    l++;
                    r--;
                }
            }

            if (left < r)
            {
                QuickSort(ref data, left, r);
            }
            if (l < right)
            {
                QuickSort(ref data, l, right);
            }
        }

        static void StackSort(ref List<int> data)
        {
            int amountOfStacks = (data.Count >> 20) + 1;
            List<int>[] stacks = new List<int>[amountOfStacks]; // makes stacks, in which the data will be split

            for (int i = 0; i < amountOfStacks; i++)
                stacks[i] = new List<int>(1 << 20);

            for (int i = 0; i < data.Count; i++)
            {
                stacks[i >> 20].Add(data[i]); // adds the data to the stacks
            }
            
            for (int i = 0; i < amountOfStacks; i++)
            {
                stacks[i].Sort(); // sort the stacks
            }

            int[] counters = new int[amountOfStacks]; // make counters for the stacks, so you know which element is on top
            int minData;
            int minIndex;
            for (int i = 0; i < data.Count; i++)
            {
                minIndex = 0;
                while (counters[minIndex] == stacks[minIndex].Count) // checks if the stack is "empty"
                {
                    minIndex++;
                }
                minData = stacks[minIndex][counters[minIndex]]; // sets the comparison data

                for (int j = 0; j < amountOfStacks; j++)
                {
                    if (counters[j] < stacks[j].Count && stacks[j][counters[j]] < minData) // checks if the stack is not "empty" and if the data is smaller
                    {
                        minData = stacks[j][counters[j]];
                        minIndex = j;
                    }
                }
                data[i] = minData; // set the data in the output list
                counters[minIndex]++; // increase the counter of the stack, so that the next element is "on top"
            }
        }
    }
}
